DNA sequences are double-stranded (helicoidal) structures composed of four nucleotides A ( adenine), C (
cytosine), G (guanine) and T (thymine), paired A−T, C−G according to the so-called Watson-Crick complements. If we have
a single stranded sequence of A,C,G,Т nucleotides, together with a single stranded sequence composed of the
complementary nucleotides, the two sequences will be " glued" together (by hydrogen bonds, forming a double-stranded DNA
sequence. This matching of complementary nucleotides now is the constraint which has to be fulfilled when we prolong to
the left and to the right a sequence of ( single or double) symbols by using given single stranded strings or even more
complex dominoes with sticky ends gluing these ends together with the sticky ends of the current sequence according to a
complementarity relation.
