---
title: Bidirectional Sticker Systems
---
There exist several types of bidirectional sticker systems that represent regular languages, one of which is found to
represent the linear languages. Another variant is proved to be able to represent any recursively enumerable language,
which reminds the results obtained in a series of papers 2;4; about the possibility of designing universal (and
programmable) DNA "computers" based on the operation of splicing.
