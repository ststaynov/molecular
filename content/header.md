---
title: Sticker Systems
---
Language-generating devices based on the sticker operation representing a computability model introduced as an
abstraction of Adleman's style of DNA computing
