module.exports = {
  purge: [],
  theme: {
    extend: {},
  },
  variants: {
    opacity: ({ after }) => after(['disabled']),
    cursor: ({ after }) => after(['disabled']),
  },
  plugins: [],
}
